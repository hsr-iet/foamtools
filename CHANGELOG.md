# Changelog

<!--next-version-placeholder-->

## v0.2.1 (2022-02-07)
### Fix
* Remove import of foamtools in doc generation ([`1f5d218`](https://gitlab.com/hsr-iet/foamtools/-/commit/1f5d218099259d0c0be782ddd98e1a7b2b20b35c))

## v0.2.0 (2022-02-07)
### Feature
* Add new method for creating the openfoam config files ([`220bfe9`](https://gitlab.com/hsr-iet/foamtools/-/commit/220bfe98a772d71873fc245a2245bc5f1bb77dd7))
* Add also positions to vector output ([`962fd60`](https://gitlab.com/hsr-iet/foamtools/-/commit/962fd600690bb8be7304d50877ad474783ff9f16))

### Fix
* Do not generate log folder automatically and by default of any submit script ([`949a9f8`](https://gitlab.com/hsr-iet/foamtools/-/commit/949a9f8e49ac56a71ea6340cef4db6637359d589))
* Correct dependency to basicio ([`5ea3013`](https://gitlab.com/hsr-iet/foamtools/-/commit/5ea3013f8149561b0bbc9e948654acc43f0f4576))
* Use case_dir instead of case_name ([`b702184`](https://gitlab.com/hsr-iet/foamtools/-/commit/b7021847c92bcd7a825a38c99f5f42f7d3de8095))
* Correct typo in blockMesh ([`014be44`](https://gitlab.com/hsr-iet/foamtools/-/commit/014be44162f23677420860352ff39cbcbe28cb56))
* Use default controlDict for geometry manipulation ([`c73d3a4`](https://gitlab.com/hsr-iet/foamtools/-/commit/c73d3a407777518e04201fe6520493151b2d4e78))

### Documentation
* Add init information to doc config ([`282d003`](https://gitlab.com/hsr-iet/foamtools/-/commit/282d0037de9c92577989c071cbcad1bcc236b26f))
