
====================
foamTools repository
====================

This is the repository for automating OpenFOAM.

For more information please contact the maintainers:

.. code::

   Adrian Rohner <adrian.rohner@hsr.ch>
   Juan Pablo Carbajal <juan.pablo.carbajal@hsr.ch>

Testing
-------
For the tests to run properly you need some test data in ``data/test_data``:

.. code::

    .
    ├── dropVolFlow
    ├── p_rgh
    ├── surfaceFieldValueScalar.dat
    └── surfaceFieldValueVector.dat
