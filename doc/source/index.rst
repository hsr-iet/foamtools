.. test documentation master file, created by
   sphinx-quickstart on Wed Nov  6 07:15:03 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BasicIO's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   foam2py
   foamsub
   post
   py2foam
   settingadapter

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
