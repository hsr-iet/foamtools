# vim: set tabstop=4
# foam2py.py
# !/usr/bin/env python3

"""
OpenFOAM FOAM to Python converter.

This module is used to convert input files from OpenFOAM to Python readable input.

authors:
    - adrian rohner <adrian.rohner@hsr.ch>
    - Juan Pablo Carbajal <juan.pablo.carbajal@hsr.ch>
"""
import errno
import logging
import numpy as np
from pathlib import Path

logging.getLogger(__name__).addHandler(logging.NullHandler())
logger = logging.getLogger(__name__)

field_units = dict(p='pressure (Pa)',
                   p_rgh='pressure (Pa)',
                   rho='density (kg/m^3)',
                   dropVolFlow='volume flow (m^3/s)',
                   surfaceFieldValue='volume flow (m^3/s)',
                   T='temperature (K)',
                   T_air='temperature (K)',
                   T_fluid='temperature (K)')


def vector2list(vectorstr):
    """ Convert openfoam vector string to list
    
    An openfoa vector is a sequence of numbers separated by
    spaces and enclosed by (), e.g. (1 2 3)

    Arguments
    ---------
    vectorstr : str
        Vector as string.
    Returns
    -------
    list
        Returns a list of vector indices.
    """

    vectorstr = vectorstr.strip().split()
    return list(eval(','.join(vectorstr)))


def readscalarprobes(fname):
    """ Read scalar probes from openfoam postprocessing files

    Arguments
    ---------
    fname : str
        Full file path.
    Returns
    -------
    :py:class:`numpy.ndarray`
        Returns position, time and values as array.
    """

    position = list()
    time = list()
    values = list()
    # Parse header for positions
    with open(fname) as file:
        logger.debug("parsing probes:")
        for line in file:
            # read probe position
            if '# Probe' in line:
                logger.debug("parsed line is: %s" % line.split())
                _, _, probeid, x, y, z = line.split()
                logger.debug("x=%s, y=%s, z=%s" % (x[1:], y, z[:-1]))
                if x[0] != '(':
                    # Probes parsed
                    pass
                probeid = int(probeid)
                x = float(x[1:])  # (
                y = float(y)
                z = float(z[:-1])  # )\n
                position.append([x, y, z])
            elif '#' not in line:
                t_values = list(map(float, line.split()))
                time.append(t_values[0])
                values.append(t_values[1:])

    position = np.array(position)
    time = np.array(time)
    values = np.array(values)

    return position, time, values


def readscalar(fname):
    """ Read a single scalar from openfoam postprocessing files

    Arguments
    ---------
    fname : str
        Full file path.
    Returns
    -------
    :py:class:`numpy.ndarray`
        Returns x and y values as array.
    """

    x_values = list()
    y_values = list()
    with open(fname) as file:
        for line in file:
            # read header line
            if '#' in line:
                logger.debug("parsing header:")
                logger.debug("parsed line is: %s" % line.split())
                _, x_str, y_str = line.split()
                logger.debug("x-string=%s, y-string=%s" % (x_str, y_str))
            elif '#' not in line:
                str_list = line.split()
                if len(str_list) == 1:
                    # string not been split because of too long values connecting by - sign
                    str_list = line.split('-')
                    if len(str_list) == 3:
                        # only y value has been split
                        x_values.append(float(str_list[0]))
                        val = '-' + str_list[1] + '-' + str_list[2]
                        y_values.append(float(val))
                    elif len(str_list) == 4:
                        # x value seems also to have - sign
                        val = str_list[0] + '-' + str_list[1]
                        x_values.append(float(val))
                        val = '-' + str_list[2] + '-' + str_list[3]
                        y_values.append(float(val))
                else:
                    x_values.append(float(str_list[0]))
                    y_values.append(float(str_list[1]))

    x_values = np.array(x_values)
    y_values = np.array(y_values)

    return x_values, y_values


def readofvecdat(fname):
    """ Read OpenFOAM .dat from openfoam postprocessing files

    Arguments
    ---------
    fname : str
        Full file path.
    Returns
    -------
    :py:class:`numpy.ndarray`
        Returns time, value and meta data as array.
    """
    time = list()
    values = list()
    positions = list()
    metadata = dict()
    # Parse header for positions
    if not Path(fname).exists():
        raise FileNotFoundError(errno.ENOENT, fname)

    with open(fname) as file:
        for line in file:
            # read header
            if ':' in line:
                logger.debug("parsing header line: %s" % line)
                field, data = line.split(':', maxsplit=1)
                logger.debug("parsed line is: %s" % line.split(':', maxsplit=1))
                if 'Region' in field:
                    regtype, name = data.split()
                    logger.debug("region type is: %s" % regtype)
                    logger.debug("region name is: %s" % name)
                    metadata['type'] = regtype
                    metadata['name'] = name
                elif 'Area' in field:
                    metadata['area'] = float(data)
                    logger.debug("area is: %s" % metadata['area'])
                elif 'Faces' in field:
                    metadata['faces'] = int(data)
                    logger.debug("faces are: %s" % metadata['faces'])
            if '# Probe' in line:
                logger.debug("parsed line is: %s" % line.split())
                _, _, probeid, x, y, z = line.split()
                logger.debug("x=%s, y=%s, z=%s" % (x[1:], y, z[:-1]))
                if x[0] != '(':
                    # Probes parsed
                    pass
                probeid = int(probeid)
                x = float(x[1:])  # (
                y = float(y)
                z = float(z[:-1])  # )\n
                positions.append([x, y, z])
            elif '#' not in line:
                t, v = line.split(maxsplit=1)
                time.append(float(t))
                values.append(vector2list(v))


    time = np.array(time)
    values = np.array(values)
    return time, values.T, metadata, positions


def readofdat(fname):
    """ Read OpenFOAM .dat from openfoam postprocessing files

    Arguments
    ---------
    fname : str
        Full file path.
    Returns
    -------
    :py:class:`numpy.ndarray`
        Returns time, value and meta data as array.
    """
    time = list()
    values = list()
    metadata = dict()
    # Parse header for positions
    with open(fname) as file:
        for line in file:
            # read header
            if ':' in line:
                logger.debug("parsing header line: %s" % line)
                field, data = line.split(':', maxsplit=1)
                logger.debug("parsed line is: %s" % line.split(':', maxsplit=1))
                if 'Region' in field:
                    regtype, name = data.split()
                    logger.debug("region type is: %s" % regtype)
                    logger.debug("region name is: %s" % name)
                    metadata['type'] = regtype
                    metadata['name'] = name
                elif 'Area' in field:
                    metadata['area'] = float(data)
                    logger.debug("area is: %s" % metadata['area'])
                elif 'Faces' in field:
                    metadata['faces'] = int(data)
                    logger.debug("faces are: %s" % metadata['faces'])
            elif '#' not in line:
                t, v = line.split()
                time.append(float(t))
                values.append(float(v))

    time = np.array(time)
    values = np.array(values)
    return time, values, metadata


def readflow(fname):
    """ Read flowRatePatch data from openfoam postprocessing files

    Arguments
    ---------
    fname : str
        Full file path.
    Returns
    -------
    :py:class:`numpy.ndarray`
        Returns time, value and meta data as array.
    """
    time = list()
    values = list()
    metadata = dict()
    # Parse header for positions
    with open(fname) as file:
        for line in file:
            # read header
            if ':' in line:
                field, data = line.split(':', maxsplit=1)
                if 'Region' in field:
                    regtype, name = data.split()
                    metadata['type'] = regtype
                    metadata['name'] = name
                elif 'Area' in field:
                    metadata['area'] = float(data)
                elif 'Faces' in field:
                    metadata['faces'] = int(data)
            elif '#' not in line:
                t, v = line.split()
                time.append(float(t))
                values.append(float(v))

    time = np.array(time)
    values = np.array(values)
    return time, values, metadata


def readheightprobes(fname):
    """ Read interface height probes from openfoam postprocessing files

    Arguments
    ---------
    fname : str
        Full file path.
    Returns
    -------
    :py:class:`numpy.ndarray`
        Returns Location, time, h_above_location, h_above_boundary.
    """
    location = list()
    time = list()
    h_above_boundary = list()
    h_above_location = list()
    # Parse header for positions
    with open(fname) as file:
        for line in file:
            # read probe location
            if ':' in line and 'Location' in line:
                _, vecstr = line.split(':', maxsplit=1)
                location.append(vector2list(vecstr))
            elif '#' not in line:
                t_values = list(map(float, line.split()))
                time.append(t_values[0])
                h_above_boundary.append(t_values[1::2])
                h_above_location.append(t_values[2::2])

    location = np.array(location)
    time = np.array(time)
    h_above_boundary = np.array(h_above_boundary)
    h_above_location = np.array(h_above_location)

    return location, time, h_above_location, h_above_boundary


def readvariable(fname, varname):
    """ Read variable

    Arguments
    ---------
    fname : str
        Full file path.
    varname : str
        Variable name.
    Returns
    -------
    str
        Returns the string value of the variable.
    """
    valuestr = list()
    varfound = False
    with open(fname) as file:
        for line in file:
            if varname in line:
                varfound = True
                if ';' in line:
                    valuestr = line.split()
                    valuestr = [valuestr[-1].split(';')[0]]  # ends with ;
                    break
            elif varfound:
                if ';' in line:
                    valuestr.append(line[:-1].split(';')[0])
                    break
                else:
                    valuestr.append(line.strip())
    return valuestr
