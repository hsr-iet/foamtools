
def create_foam_sub_script(*, job_name=None, mesh=True, init=True, sing_run=True, par_run=False, hpc=False, log=True):

    log_folder = 'logFiles'
    bash_header_str = '#!/bin/bash'
    change_dir_str = get_change_dir_cmd()


    if log:
        logging_str = get_init_logging_cmd(log_dir=log_folder)
    else:
        logging_str = ''

    if mesh:
        mesh_pipeline_str = get_foam_sub_mesh(log_folder)
    else:
        mesh_pipeline_str = ''

    if init:
        init_pipeline_str = get_foam_sub_init(log_folder)
    else:
        init_pipeline_str = ''

    if par_run:
        run_pipeline_str = get_foam_sub_parallel_run(log_folder)
    elif hpc:
        run_pipeline_str = get_foam_sub_cluster_run(job_name=job_name)
    elif sing_run:
        run_pipeline_str = get_foam_sub_single_run(log_folder)
    else:
        run_pipeline_str = ''

    sub_str = bash_header_str + change_dir_str + logging_str + mesh_pipeline_str + init_pipeline_str + run_pipeline_str
    return sub_str


def get_foam_sub_mesh(log_folder):

    mesh_pipeline_str = get_mesh_pipeline_header()
    mesh_pipeline_str += get_mesh_controls_cmd()
    mesh_pipeline_str += get_block_mesh_cmd(log_dir=log_folder)
    mesh_pipeline_str += get_surf_feature_extract_cmd(log_dir=log_folder)
    mesh_pipeline_str += get_snappy_hex_mesh_cmd(log_dir=log_folder)
    mesh_pipeline_str += get_extrude_mesh_cmd(log_dir=log_folder)
    mesh_pipeline_str += get_change_dictionary_cmd()
    mesh_pipeline_str += get_create_patch_cmd()
    mesh_pipeline_str += get_mesh_stats_cmd(log_dir=log_folder)
    mesh_pipeline_str += get_pipeline_endblock()

    return mesh_pipeline_str


def get_foam_sub_init(log_folder):

    init_pipeline_str = get_init_pipeline_header()
    init_pipeline_str += get_zero_dir_cmd()
    init_pipeline_str += get_point_displacement_cmd()
    init_pipeline_str += get_set_fields_cmd(log_dir=log_folder)
    init_pipeline_str += get_rm_stuff_cmd()
    init_pipeline_str += get_pipeline_endblock()

    return init_pipeline_str


def get_foam_sub_single_run(log_folder):

    run_pipeline_str = get_run_pipeline_header()
    run_pipeline_str += get_run_control_cmd()
    run_pipeline_str += get_run_cmd()
    run_pipeline_str += get_pipeline_endblock()

    return run_pipeline_str


def get_foam_sub_cluster_run(job_name):

    run_pipeline_str = get_run_pipeline_header()
    run_pipeline_str += get_run_control_cmd()
    run_pipeline_str += get_run_cluster_cmd(job_name=job_name)
    run_pipeline_str += get_pipeline_endblock()

    return run_pipeline_str


def get_foam_sub_parallel_run(log_folder):

    run_pipeline_str = get_run_pipeline_header()
    run_pipeline_str += get_run_control_cmd()
    run_pipeline_str += get_decompose()
    run_pipeline_str += get_run_par(n_cores=2)
    run_pipeline_str += get_reconst()
    run_pipeline_str += get_rm_proc_dir()
    run_pipeline_str += get_pipeline_endblock()

    return run_pipeline_str


def get_change_dir_cmd():
    """ Command line for changing into the run script, where the bash has been executed """
    change_dir_str = \
        """
# The command cd ${0%/*} changes directory to the directory containing the script, 
# assuming that $0 is set to the fully-qualified path of the script. 
# Hence, this is very important to run this bash script from the case folder!
clear
cd ${0%/*} || exit 1
        """
    return change_dir_str


def get_mesh_pipeline_header():
    """ Comment line for mesh header pipeline """
    mesh_pipeline_str = \
        """
echo -e "\n
Start meshing
-------------------------------------------------------------------------------------
"
        """
    return mesh_pipeline_str


def get_init_pipeline_header():
    """ Comment line for initialize header pipeline """
    run_pipeline_str = \
        """
echo -e "\n
Initialize Case
-------------------------------------------------------------------------------------
"
        """
    return run_pipeline_str


def get_run_pipeline_header():
    """ Comment line for run header pipeline """
    run_pipeline_str = \
        """
echo -e "\n
Run Case
-------------------------------------------------------------------------------------
"
        """
    return run_pipeline_str


def get_pipeline_endblock():
    """ Comment line for end pipeline """
    end_block_str = \
        """
echo -e "\n
-------------------------------------------------------------------------------------
"
        """
    return end_block_str


def get_init_logging_cmd(*, log_dir):
    """ Command line for creating the log folder """
    logging_str = \
        """
echo -e "   - create log folder"
rm -rf log*
mkdir """
    logging_str += log_dir
    return logging_str


def get_mesh_controls_cmd():
    """ Command line for setting up the right control dicts for the mesh process """
    mesh_control_cmd = \
        """
echo -e "   - copy fvSolution file to system"
cp system/fvSolution{InitFixedPlunger,}
        """
    return mesh_control_cmd


def get_block_mesh_cmd(*, log_dir):
    """ Command line for block mesh """
    block_mesh_cmd = \
        """
echo -e "   - create background mesh"
blockMesh """
    if log_dir:
        block_mesh_cmd += '> ' + log_dir + '/' + 'blockMesh.log'
    return block_mesh_cmd


def get_extrude_mesh_cmd(*, log_dir):
    """ Command line for extrude mesh """
    extrude_mesh_cmd = \
        """
echo -e "   - extrude one patch to make a 2D rotational mesh"
extrudeMesh """
    if log_dir:
        extrude_mesh_cmd += '> ' + log_dir + '/' + 'extrudeMesh.log'
    return extrude_mesh_cmd


def get_snappy_hex_mesh_cmd(*, log_dir):
    """ Command line for snappy mesh, flag set to overwrite """
    snappy_mesh_cmd = \
        """
echo -e "   - meshing with snappyHexMesh"
snappyHexMesh -overwrite """
    if log_dir:
        snappy_mesh_cmd += '> ' + log_dir + '/' + 'snappyMesh.log'
    return snappy_mesh_cmd


def get_surf_feature_extract_cmd(*, log_dir):
    """ Command line for surface feature extract """
    surf_feature_ext_cmd = \
        """
echo -e "   - create surface feature edges"
surfaceFeatureExtract """
    if log_dir:
        surf_feature_ext_cmd += '> ' + log_dir + '/' + 'surfFeatExt.log'
    return surf_feature_ext_cmd


def get_change_dictionary_cmd():
    """ Command line for change dictionary """
    change_dictionary_cmd = \
        """
echo -e "   - change boundary type to wedge (polyMesh/boundary)" 
changeDictionary  >> tmpFile.log
        """
    return change_dictionary_cmd


def get_create_patch_cmd():
    """ Command line for create patch, flag is set to overwrite """
    create_patch_cmd = \
        """
echo -e "   - remove not needed patch entries"
createPatch -overwrite >> tmpFile.log
        """
    return create_patch_cmd


def get_mesh_stats_cmd(*, log_dir):
    """ Command line for check mesh """
    mesh_stats_cmd = \
        """
echo -e "   - write mesh stats"
checkMesh """
    if log_dir:
        mesh_stats_cmd += '> ' + log_dir + '/' + 'checkMesh.log'
    return mesh_stats_cmd

# todo: change the .foam file to the actual case name
def get_zero_dir_cmd():
    """ Command line for copying zero directory an creating a paraview file with .foam """
    zero_dir_cmd = \
        """
echo -e "   - create 0 directory"
rm -r 0
cp -r 0.orig 0 """
    return zero_dir_cmd


# todo: this is actually magtip specific --> change module
def get_point_displacement_cmd():
    """ Command line for copying point displacement to zero """
    point_displ_cmd = \
        """
echo -e "   - copy pointDisplacement file to 0"
cp 0/pointDisplacement{InitFixedPlunger,}
rm 0/pointDisplacementInitFixedPlunger
rm 0/pointDisplacementInitOffsetPlunger"""
    return point_displ_cmd


def get_set_fields_cmd(*, log_dir):
    """ Command line for set fields """
    fields_cmd = \
        """
echo -e "   - create fields"
setFields """
    if log_dir:
        fields_cmd += '> ' + log_dir + '/' + 'setFields.log'
    return fields_cmd


def get_rm_stuff_cmd():
    """ Command line for removing tmp* stuff """
    rm_stuff_cmd = \
        """
echo -e "   - remove not needed stuff"
rm -r tmp*"""
    return rm_stuff_cmd


def get_run_control_cmd():
    """ Command line for setting up the right control dicts for the run process """
    run_control_cmd = \
        """
echo -e "   - copy controlDict file for run (ascii) to system"
cp system/controlDict{InitFixedPlunger,}"""

    return run_control_cmd


def get_run_cmd(of_solver='compressibleInterFoam'):
    """ Command line for single core run """
    run_single_cmd = \
        """
echo -e "   - run simulation local serial"
{solver} > {solver}.log
echo "run completed successfully" > finFlag"""
    return run_single_cmd.format(solver=of_solver)


def get_run_par(*, n_cores, of_solver='compressibleInterFoam'):
    """ Command line for parallel/multi core run """
    run_par_cmd = \
        """
echo -e "   - run simulation local parallel on {cores} cores"
mpirun -np {cores} {solver} -parallel > {solver}.log
echo "run completed successfully" > finFlag"""
    return run_par_cmd.format(cores=n_cores, solver=of_solver)


def get_run_cluster_cmd(job_name=None, of_solver='compressibleInterFoam', of_version=7):
    """ Command line for single core run on cluster """
    if not job_name:
        job_name = of_solver

    run_single_cmd = \
        """
echo -e "   - load psub scripts"
shopt -s expand_aliases
source /etc/profile.d/submitScripts.sh
echo $?
echo -e "   - run simulation local serial"
psub-openfoam -s {solver} -sc -v {version} -j {jobname}"""

    return run_single_cmd.format(solver=of_solver, version=of_version, jobname=job_name)


def get_decompose():
    """ Command line for decomposing the domain """
    decomp_cmd = \
        """
echo -e "   - decompose domain"
decomposePar >> logFiles/decPar.log"""
    return decomp_cmd


def get_reconst():
    """ Command line for reconstructing the domain """
    reconst_cmd = \
        """
echo -e "   - reconstruct domain"
reconstructPar >> logFiles/recPar.log"""
    return reconst_cmd


def get_rm_proc_dir():
    """ Command line for removing the decomposed folders """
    rm_proc_cmd = \
        """
echo -e "   - remove old processor directories"
rm -rf processor*"""
    return rm_proc_cmd
