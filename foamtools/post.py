#!/usr/bin/env python
# -*- coding: utf-8 -*-

from foamtools import py2foam

"""OpenFOAM post processing-files preparation.

This module is used to setup postprocessing files like probes, surfaces, etc.

authors:
    - adrian rohner <adrian.rohner@hsr.ch>
    - Juan Pablo Carbajal <juan.pablo.carbajal@hsr.ch>
"""


def probefile(*, probe_list, field_list, write_control='timeStep', write_interval=1):
    """ Set probe locations and fields as well as write control and interval

    Parameters
    ----------
    probe_list: list
        List with probe locations
    field_list: list
        List with the probe fields, e.g. rho, p_rgh, T.air, etc.
    write_control: str
        Control of the write method for probes, default is timeStep.
    write_interval: float
        Write interval for probes, default is 1, hence each time step.

    Returns
    -------
    strfile: str
        String with the setting for openFOAM probe file.

    """

    # Header
    headertemplate = py2foam.of_header + \
                     """
/*---------------------------------------------------------------------------
Description
    Writes out values of fields from cells nearest to specified locations.

---------------------------------------------------------------------------*/
#includeEtc "caseDicts/postProcessing/probes/probes.cfg"

writeControl   {wcontrol};
writeInterval  {winterval};
        """

    fields_of_str = py2foam.list2foam(field_list, name='fields') + ';'
    probe_loc_str = py2foam.list2foam(probe_list, name='probeLocations') + ';'

    strfile = headertemplate.format(wcontrol=write_control, winterval=write_interval) \
              + '\n' + fields_of_str + '\n' + probe_loc_str

    return strfile


def surfacefile(*, surf_name, surf_type, field_list):
    """ Set probe locations and fields as well as write control and interval

    Parameters
    ----------
    surf_name : str
        Name of the surface.
    surf_type : str
        A given surface type from surfaces.cfg.
    field_list : list
        List of fields to evaluate on the surface.

    Returns
    -------
    strfile: str
        String with the setting for openFOAM surface file.

    """

    # Header
    headertemplate = py2foam.of_header + \
                     """
/*---------------------------------------------------------------------------
Description
    Writes out surface files with interpolated field data in VTK format, e.g.
    cutting planes, iso-surfaces and patch boundary surfaces.

    This file includes a selection of example surfaces, each of which the user
    should configure and/or remove.

---------------------------------------------------------------------------*/
#include "surfaces.cfg"
        """

    surf_definition = \
        """
surfname   {surfname};
surftype  ${surftype};

        """

    surf_body= \
        """
surfaces
(
    $surfname
    {
        $surftype;
        pointAndNormalDict
        {
            basePoint    (0 0 0); // Overrides default basePoint (0 0 0)
            normalVector $z;      // $y: macro for (0 0 1)
        }
    }
);
        """

    fields_of_str = py2foam.list2foam(field_list, name='fields') + ';'
    strfile = headertemplate + '\n' + fields_of_str + '\n' + \
              surf_definition.format(surfname=surf_name, surftype=surf_type) + '\n' + surf_body

    return strfile


def remove_resfolders(case_name):
    pass


def copy_resultfolder(case_name, dst_path):
    pass
