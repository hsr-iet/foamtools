# vim: set tabstop=4
# py2foam.py
# !/usr/bin/env python3

"""
OpenFOAM Python to FOAM converter.

This module is used to convert input files from Python to OpenFOAM readable input.

authors:
    - adrian rohner <adrian.rohner@hsr.ch>
    - Juan Pablo Carbajal <juan.pablo.carbajal@hsr.ch>
"""

import os
import logging
from pathlib import Path
from typing import Any, Dict, List, Union

logger = logging.getLogger(__name__)

# standard openFOAM header
of_header = \
    """
/*--------------------------------*- C++ -*------------------------------------
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Web:      www.OpenFOAM.org
     \\/     M anipulation  |
-----------------------------------------------------------------------------*/
        """


def dict2foam(dic, *, name, pad=''):
    """ Convert a dictionary to a string representation compatible with
    OpenFoam """
    strfield = '    {field}  {value};'
    strbody = []
    for k, v in dic.items():
        if isinstance(v, dict):
            vstr = dict2foam(v, name=k, pad=pad + '    ')
            strbody.append(vstr)
        else:
            strbody.append(pad + strfield.format(field=k, value=v))
    strbody = '\n'.join(strbody)
    strdict = '{p}{name}\n{p}{{\n{body}\n{p}}}'.format(
        name=name, body=strbody, p=pad)
    return strdict


def list2foam(ls, *, name, pad=''):
    """ Convert a list to a named list representation compatible with OpenFoam """

    strbody = []
    for v in ls:
        if isinstance(v, (tuple, list)):
            # do this only if contents are not iterables
            # if any(map(isinstance, v, (tuple, list))):
            #     strbody.append(pad + '    ' + list2foam(v, name=''))
            strbody.append(pad + '    ' + iter2foam(v))
        else:
            strbody.append(pad + '    ' + str(v))
    strbody = '\n'.join(strbody)
    strls = '{p}{name}\n{p}(\n{body}\n{p})'.format(
        name=name, body=strbody, p=pad)
    return strls


def iter2foam(tp):
    """ Convert a list or tuple with non-iterables elements to a string
    representation compatible with OpenFoam """

    tbl = ''.maketrans({',': ' ', '[': '(', ']': ')'})
    return str(tp).translate(tbl)


def compose_region_stl(*, case_dir: Union[str, Path], stl_dir: Union[str, Path] = "stl"):
    """Compose stl file from single stl bc's.

    Arguments
    ---------
    case_dir : :py:class:`str`
        Name of the case path.
    stl_dir : :py:class:`str`
        Name of the stl folder with bc's.

    Returns
    -------
    bool
        True if successful.

    Raises
    ------
    FileNotFoundError
        If source file not found or target bc file not given.


    """
    if isinstance(case_dir, str):
        case_dir = Path(case_dir)

    stl_file_name = 'regionSTL'

    source_stl_path = stl_dir

    case_stl_region_path = case_dir.joinpath('constant/triSurface')
    logger.debug("current path is: %s" % os.getcwd())
    logger.debug("given STL-path is: %s" % source_stl_path)
    logger.debug("given stl case path is: %s" % case_stl_region_path)

    bc_list = []
    try:
        if os.listdir(source_stl_path):
            for file in os.listdir(source_stl_path):
                file_name = os.path.splitext(file)[0]
                file_ext = os.path.splitext(file)[1]
                if file_ext == '.stl':
                    bc_list.append(_add_boundary(source_stl_path, file, 'solid ' + file_name))
        else:
            raise FileNotFoundError("no boundary stl files found!")
        logger.debug("create boundary collection file: %s" % Path(case_stl_region_path, stl_file_name))
        with open('%s/%s.stl' % (case_stl_region_path, stl_file_name), 'w') as boundary_collection_file:
            boundary_collection_file.writelines(bc_list)
        return True
    except FileNotFoundError as err:
        logger.error(err)
        raise


def _add_boundary(file_path: Union[str, Path], src_filename, replacement_line):
    """Reads a boundary stl and puts the file name at the beginning of the file.

    Arguments
    ---------
    file_path : :py:class:`str`
        path to stl file.
    src_filename : :py:class:`str`
        the name of the boundary file.
    replacement_line : :py:class:`str`
        first line of the stl to be replaced.

    Returns
    -------
    str of boundary description in stl format.

    See also
    --------
    :py:func:`.compose_region_stl`

    """
    logger.debug("get boundary: %s" % Path(file_path, src_filename))
    boundary_file = open(Path(file_path, src_filename), 'r')
    first_line, remainder = boundary_file.readline(), boundary_file.read()
    boundary = replacement_line + "\n" + remainder
    boundary_file.close()

    return boundary


def create_displacement_input_file(*, include_dir: Union[str, Path], time_data, pos_data):
    """Creates the displacement file used by OpenFOAM to move the mesh bc.

    Arguments
    ---------
    include_dir : str
        Path to case include dir.
    time_data : str
        Time serie of plunger movement.
    pos_data : str
        Position serie of plunger movement.
    """
    # leave x and z position to zero
    x_pos = '0'
    z_pos = '0'

    # open file for writing
    f = open('%s/PistonPos.csv' % include_dir, 'w')

    # write new file
    sep = ','
    for i in range(0, len(time_data)):
        f.write(str(time_data[i]) + sep + x_pos + sep + str(pos_data[i]) + sep + z_pos + '\n')

    f.close()


def create_dot_foam(case_dir: Union[str, Path], case_name):
    """Creates the case .foam file for reading with paraview.

    Arguments
    ---------
    case_dir : :py:class:`str`
        Path to case.
    case_name : :py:class:`str`
        Name of the case or .foam file.
    """
    try:
        open('{}.foam'.format(Path(case_dir, case_name)), mode='w').close()
    except FileNotFoundError:
        logger.error('case does not exist! no .foam file created!')
