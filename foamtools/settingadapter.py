import logging
from abc import ABC
from os.path import join
from pathlib import Path
from typing import Union

from basicio.file import write_dict2file

logger = logging.getLogger(__name__)

# todo: deprecated class, removed by create_foam_config
class OFConfig(ABC):
    include_dir = None

    def __init__(self, *, params):
        for name, value in params.items():
            setattr(self, name, value)

    def asdict(self):
        return self.__dict__

    def print_info(self):
        params = self.__dict__
        print('--------------------------------')
        for name, value in params.items():
            print('{: <20}'.format(name + ':'), value)
        print('--------------------------------')

    def create_init_file(self, *, init_name):
        if not self.include_dir:
            raise SystemExit("include dir not set")
        else:
            write_dict2file(join(self.include_dir, init_name), self.asdict(), comment_mark='//')


class SimulationControl(OFConfig):

    def __init__(self, *, params):
        for name, value in params.items():
            if name == 'date':
                logger.info('Date field found, which introduces format errors within openFOAM. \n'
                            'Date will not be added to openfoam config!')
            else:
                setattr(self, name, value)


Fluid = OFConfig
SolverControl = OFConfig
SimControl = SimulationControl
MeshSetting = OFConfig
InitialCondition = OFConfig
Geometry = OFConfig


def create_foam_config(config_dict: dict, file_path: Union[str, Path]):
    write_dict = dict()

    for key, value in config_dict.items():
        if key == 'date':
            logger.warning('Date field found, which introduces format errors within openFOAM. \n'
                           'Date will not be added to openfoam config!')
        else:
            write_dict.update({key: value})
    write_dict2file(file_path, write_dict, comment_mark='//')


if __name__ == '__main__':
    fluid_params = dict(name='hvl', density=997, viscosity=0.001, surf_tens_coef=0.072)
    new_fluid = Fluid(params=fluid_params)
    new_fluid.print_info()
