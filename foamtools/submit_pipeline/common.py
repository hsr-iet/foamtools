""" Standard pipeline methods

"""

logfile_dir = 'logFiles'


def get_subscript_init():
    sub_str = get_shebang()
    sub_str += get_change_to_bashexec_dir()
    return sub_str


def get_custom_cmd(description, cmd):
    """ Custom command output """
    custom_cmd = \
        f"""
echo -e "   - {description}"
{cmd}"""
    return custom_cmd


def get_custom_message(output):
    """ Custom message output"""
    pipeline_str = \
        f"""
echo -e "{output}"
"""
    return pipeline_str


def get_create_log_dir():
    """ Command line for creating the log folder """

    desc = 'create log folder'
    cmd = 'rm -rf log* \n' \
          f'mkdir {logfile_dir}'

    return get_custom_cmd(desc, cmd)


def get_change_to_bashexec_dir():
    """ Command line for changing into the run script, where the bash-command/script has been executed """

    desc = 'change dir to bash execution path'
    cmd = 'cd ${0%/*} || exit 1'

    return get_custom_cmd(desc, cmd)


def get_shebang(shebang='#!/bin/bash'):
    return shebang

def get_pipeline_header(header_name):
    """ Comment line for header pipeline """
    pipeline_str = \
        f"""
echo -e "\n
{header_name}
-------------------------------------------------------------------------------------"
"""
    return pipeline_str


def get_pipeline_endblock():
    """ Comment line for end pipeline """
    end_block_str = \
        """
echo -e "\n
-------------------------------------------------------------------------------------"
"""
    return end_block_str
