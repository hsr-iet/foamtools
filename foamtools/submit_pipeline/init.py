"""init pipeline methods

"""

import foamtools.submit_pipeline.common as cp


def get_init_pipeline(set_fields: bool = True, init_dir: str = '0.orig') -> str:

    init_pipeline_str = cp.get_pipeline_header('Start Initialization')
    init_pipeline_str += get_init_dir(init_dir)
    if set_fields:
        init_pipeline_str += get_set_fields()
    init_pipeline_str += cp.get_pipeline_endblock()
    return init_pipeline_str


# todo: change the .foam file to the actual case name
def get_init_dir(init_dir):
    """ Command line for copying zero directory an creating a paraview file with .foam """

    desc = 'create 0 directory'
    cmd = f'rm -r 0 \n' \
          f'cp -r {init_dir} 0'

    return cp.get_custom_cmd(desc, cmd)


def get_set_fields():
    """ Command line for set fields """

    desc = 'set fields'
    cmd = f'setFields > {cp.logfile_dir}/setFields.log'

    return cp.get_custom_cmd(desc, cmd)
