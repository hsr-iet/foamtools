"""mesh pipeline methods

"""

import foamtools.submit_pipeline.common as cp


def get_mesh_pipeline(blockmesh: bool = True, surf_feat_ext: bool = True, snappy: bool = True, extrude: bool = True,
                      changedict: bool = True, create_patch: bool = True, stats: bool = True) -> str:

    mesh_pipeline_str = cp.get_pipeline_header(header_name='Start meshing')
    if blockmesh:
        mesh_pipeline_str += get_block_mesh()
    if surf_feat_ext:
        mesh_pipeline_str += get_surf_feature_extract()
    if snappy:
        mesh_pipeline_str += get_snappy_hex_mesh()
    if extrude:
        mesh_pipeline_str += get_extrude_mesh()
    if changedict:
        mesh_pipeline_str += get_change_dictionary()
    if create_patch:
        mesh_pipeline_str += get_create_patch()
    if stats:
        mesh_pipeline_str += get_mesh_stats()
    mesh_pipeline_str += cp.get_pipeline_endblock()

    return mesh_pipeline_str


def get_block_mesh():
    """ Command line for block mesh """
    desc = 'create background mesh'
    cmd = f'blockMesh > {cp.logfile_dir}/blockMesh.log'

    return cp.get_custom_cmd(desc, cmd)


def get_extrude_mesh():
    """ Command line for extrude mesh """

    desc = 'extrude from patch'
    cmd = f'extrudeMesh > {cp.logfile_dir}/extrudeMesh.log'

    return cp.get_custom_cmd(desc, cmd)


def get_snappy_hex_mesh():
    """ Command line for snappy mesh, flag set to overwrite """

    desc = 'meshing with snappyHexMesh'
    cmd = f'snappyHexMesh -overwrite > {cp.logfile_dir}/snappyMesh.log'

    return cp.get_custom_cmd(desc, cmd)


def get_surf_feature_extract():
    """ Command line for surface feature extract """

    desc = 'create surface feature edges'
    cmd = f'surfaceFeatureExtract > {cp.logfile_dir}/surfFeatExt.log'

    return cp.get_custom_cmd(desc, cmd)


def get_change_dictionary():
    """ Command line for change dictionary """

    desc = 'change boundary type to wedge (polyMesh/boundary)'
    cmd = f'changeDictionary > {cp.logfile_dir}/changeDict.log'

    return cp.get_custom_cmd(desc, cmd)


def get_create_patch():
    """ Command line for create patch, flag is set to overwrite """

    desc = 'remove not needed patch entries'
    cmd = f'createPatch -overwrite > {cp.logfile_dir}/createPatch.log'

    return cp.get_custom_cmd(desc, cmd)


def get_mesh_stats():
    """ Command line for check mesh """

    desc = 'write mesh stats'
    cmd = f'checkMesh > {cp.logfile_dir}/checkMesh.log'

    return cp.get_custom_cmd(desc, cmd)
