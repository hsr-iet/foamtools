""" run pipeline methods

"""
import foamtools.submit_pipeline.common as cp


def get_run_pipeline(job_name: str, of_solver: str = 'compressibleInterFoam',
                     of_version: int = 7, cluster: bool = False, n_cores: int = 1) -> str:

    run_pipeline_str = cp.get_pipeline_header('Initialize run')
    if not cluster:
        run_pipeline_str += get_local_run(of_solver, n_cores)
    else:
        run_pipeline_str += get_cluster_run(job_name, of_solver, of_version, n_cores)

    run_pipeline_str += cp.get_pipeline_endblock()

    return run_pipeline_str


def get_local_run(of_solver='compressibleInterFoam', n_cores=2):
    """ Command line for parallel/multi core run """
    if n_cores > 1:
        local_run_cmd = get_decompose()
        desc = f'run simulation local parallel on {n_cores} cores'
        cmd = f'mpirun -np {n_cores} {of_solver} -parallel > {of_solver}.log'
        local_run_cmd += cp.get_custom_cmd(desc, cmd)
        local_run_cmd += get_reconst()
        local_run_cmd += get_rm_proc_dir()
    else:
        desc = 'run simulation local serial'
        cmd = f'{of_solver} > {of_solver}.log'
        local_run_cmd = cp.get_custom_cmd(desc, cmd)

    return local_run_cmd


def get_cluster_run(job_name=None,of_solver='compressibleInterFoam', of_version=7, n_cores=1):
    """ Command line for single core run on cluster """

    if not job_name:
        job_name = of_solver

    desc = 'load psub scripts'
    cmd = 'shopt -s expand_aliases \n' \
          'source /etc/profile.d/submitScripts.sh'
    run_cluster_cmd = cp.get_custom_cmd(desc, cmd)
    if n_cores > 1:
        print('not yet implemented')
    else:
        desc = 'run simulation on cluster serial'
        cmd = f'psub-openfoam -s {of_solver} -sc -v {of_version} -j {job_name}'
        run_cluster_cmd += cp.get_custom_cmd(desc, cmd)

    return run_cluster_cmd


def get_decompose():
    """ Command line for decomposing the domain """
    desc = 'decompose domain'
    cmd = f'decomposePar >> {cp.logfile_dir}/decPar.log'
    return cp.get_custom_cmd(desc, cmd)


def get_reconst():
    """ Command line for reconstructing the domain """
    desc = 'reconstruct domain'
    cmd = f'reconstructPar >> {cp.logfile_dir}/recPar.log'
    return cp.get_custom_cmd(desc, cmd)


def get_rm_proc_dir():
    """ Command line for removing the decomposed folders """
    desc = 'remove old processor directories'
    cmd = 'rm -rf processor*'
    return cp.get_custom_cmd(desc, cmd)


if __name__ == '__main__':
    print(get_cluster_run())