
# See https://packaging.python.org/tutorials/packaging-projects/ for more information

from setuptools import (
    setup,
    find_packages
    )

from foamtools import (
  __version__,
  __author__,
  __email__
)

with open('README.rst', encoding='utf-8') as f:
    long_description = f.read()

setup(
      name='foamTools',
      version=__version__,
      author=__author__,
      author_email=__email__,
      description="",                   # Add short description here
      long_description=long_description,
      long_description_content_type="text/x-rst",
      url="https://gitlab.com/hsr-iet/foamtools",
      packages=find_packages(),
      python_requires='>=3.7',
      install_requires=[                # Add/remove abstract dependencies here
          ]
)

