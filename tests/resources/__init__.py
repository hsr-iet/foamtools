from pathlib import Path


def get_resource_path() -> Path:
    return Path(__file__).parent


def get_of_file(file_name):
    return get_resource_path().joinpath(f'of_files/{file_name}')
