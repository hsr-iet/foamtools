import tempfile
import unittest
import logging.config
import sys

# Local/private application/module/library imports
import resources

sys.path.append("../")  # add upper level of packages
import foamtools.foam2py as foampy
from basicio.yaml import read_yaml


log_config_path = ''                    # No logging config file, output on terminal

print('set up logger')
try:
    config = read_yaml(log_config_path)
    logging.config.dictConfig(config)
except FileNotFoundError:
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s : %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


class TestListConversion(unittest.TestCase):

    def setUp(self) -> None:
        self.test_dir = tempfile.mkdtemp()
        rsc_path = resources.get_resource_path()

    def test_read_scalarprobes(self):
        file_path = resources.get_of_file('p_rgh')
        print(file_path.resolve())
        pos, time, value = foampy.readscalarprobes(file_path)
        print(pos[0])
        print(time)
        print(value[0])

    def test_read_ofdatscalar(self):
        file_path = resources.get_of_file('surfaceFieldValueScalar.dat')
        time, drop_vol, meta = foampy.readofdat(file_path)
        print(time)
        print(drop_vol)

    def test_read_ofdat_vector(self):
        file_path = resources.get_of_file('surfaceFieldValue.dat')
        time, drop_vol, meta, pos = foampy.readofvecdat(file_path)
        self.assertIsNotNone(time)

    def test_read_ofdat_vector_file_not_found(self):
        file_path = resources.get_of_file('surfaceFieldValueNotExisting.dat')
        with self.assertRaises(FileNotFoundError):
            time, drop_vol, meta, pos = foampy.readofvecdat(file_path)

if __name__ == '__main__':
    unittest.main()
