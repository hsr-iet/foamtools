import unittest
from foamtools import py2foam


class TestListConversion(unittest.TestCase):

    def test_list2foam(self):
        foam_field = 'fields\n(\n    p\n    p_rgh\n    T.air\n)'
        fields = ['p', 'p_rgh', 'T.air']
        res = py2foam.list2foam(fields, name='fields')
        self.assertEqual(foam_field, res)

    def test_creat_dotfoam(self):
        py2foam.create_dot_foam('', 'test')

if __name__ == '__main__':
    unittest.main()
